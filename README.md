# kustomize_sample

# How to USE
stg環境のファイルを生成
```
$ oc kustomize overlays/stg
apiVersion: v1
data:
  altGreeting: staging!!!
  enableRisky: "true"
kind: ConfigMap
metadata:
  name: staging-hello-sample-configmap
---
apiVersion: v1
kind: Service
metadata:
  labels:
    app: hello-sample
  name: staging-hello-sample-srv
spec:
  ports:
  - name: 8080-tcp
    port: 8080
    protocol: TCP
    targetPort: 8080
  selector:
    app: hello-sample
  sessionAffinity: None
  type: ClusterIP
---
apiVersion: apps/v1
kind: Deployment
metadata:
  name: staging-hello-sample
spec:
  replicas: 1
  selector:
    matchLabels:
      app: hello-sample
  template:
    metadata:
      labels:
        app: hello-sample
    spec:
      containers:
      - command:
        - /hello
        - --port=8080
        - --enableRiskyFeature=$(ENABLE_RISKY)
        env:
        - name: ALT_GREETING
          valueFrom:
            configMapKeyRef:
              key: altGreeting
              name: staging-hello-sample-configmap
        - name: ENABLE_RISKY
          valueFrom:
            configMapKeyRef:
              key: enableRisky
              name: staging-hello-sample-configmap
        image: monopole/hello:1
        name: hello-sample
        ports:
        - containerPort: 8080
          protocol: TCP
---
apiVersion: route.openshift.io/v1
kind: Route
metadata:
  labels:
    app: hello-sample
  name: staging-hello-sample-route
spec:
  port:
    targetPort: 8080-tcp
  to:
    kind: Service
    name: staging-hello-sample-srv
    weight: 100
```

prod環境のファイルを生成
```
$ oc kustomize overlays/prod/
apiVersion: v1
data:
  altGreeting: proproduction!!!
  enableRisky: "true"
kind: ConfigMap
metadata:
  name: production-hello-sample-configmap
---
apiVersion: v1
kind: Service
metadata:
  labels:
    app: hello-sample
  name: production-hello-sample-srv
spec:
  ports:
  - name: 8080-tcp
    port: 8080
    protocol: TCP
    targetPort: 8080
  selector:
    app: hello-sample
  sessionAffinity: None
  type: ClusterIP
---
apiVersion: apps/v1
kind: Deployment
metadata:
  name: production-hello-sample
spec:
  replicas: 10
  selector:
    matchLabels:
      app: hello-sample
  template:
    metadata:
      labels:
        app: hello-sample
    spec:
      containers:
      - command:
        - /hello
        - --port=8080
        - --enableRiskyFeature=$(ENABLE_RISKY)
        env:
        - name: ALT_GREETING
          valueFrom:
            configMapKeyRef:
              key: altGreeting
              name: production-hello-sample-configmap
        - name: ENABLE_RISKY
          valueFrom:
            configMapKeyRef:
              key: enableRisky
              name: production-hello-sample-configmap
        image: monopole/hello:1
        name: hello-sample
        ports:
        - containerPort: 8080
          protocol: TCP
---
apiVersion: route.openshift.io/v1
kind: Route
metadata:
  labels:
    app: hello-sample
  name: production-hello-sample-route
spec:
  port:
    targetPort: 8080-tcp
  to:
    kind: Service
    name: production-hello-sample-srv
    weight: 100
[rosa@bastion kustomize_sample]$ oc kustomize overlays/prod
apiVersion: v1
data:
  altGreeting: proproduction!!!
  enableRisky: "true"
kind: ConfigMap
metadata:
  name: production-hello-sample-configmap
---
apiVersion: v1
kind: Service
metadata:
  labels:
    app: hello-sample
  name: production-hello-sample-srv
spec:
  ports:
  - name: 8080-tcp
    port: 8080
    protocol: TCP
    targetPort: 8080
  selector:
    app: hello-sample
  sessionAffinity: None
  type: ClusterIP
---
apiVersion: apps/v1
kind: Deployment
metadata:
  name: production-hello-sample
spec:
  replicas: 10
  selector:
    matchLabels:
      app: hello-sample
  template:
    metadata:
      labels:
        app: hello-sample
    spec:
      containers:
      - command:
        - /hello
        - --port=8080
        - --enableRiskyFeature=$(ENABLE_RISKY)
        env:
        - name: ALT_GREETING
          valueFrom:
            configMapKeyRef:
              key: altGreeting
              name: production-hello-sample-configmap
        - name: ENABLE_RISKY
          valueFrom:
            configMapKeyRef:
              key: enableRisky
              name: production-hello-sample-configmap
        image: monopole/hello:1
        name: hello-sample
        ports:
        - containerPort: 8080
          protocol: TCP
---
apiVersion: route.openshift.io/v1
kind: Route
metadata:
  labels:
    app: hello-sample
  name: production-hello-sample-route
spec:
  port:
    targetPort: 8080-tcp
  to:
    kind: Service
    name: production-hello-sample-srv
    weight: 100
```